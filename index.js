var btnColor = [
    "btn-primary",
    "btn-secondary",
    "btn-danger",
    "btn-warning",
    "btn-info",
    "btn-dark",
    "btn-success"
  ];

var genreCollection;
var moviesCollection;
var filteredMovies;
var userName = '#ghost';

var descTemplate = document.getElementById('description-template').innerHTML;
var $$movieCards = $('.card-columns');
var $$genresContainer = $('.genres-container');
var $$inputElement = $('#username');

$(function () {

    $('body').colorRotator({
        colors: ['#3498db','#2980b9','#2ecc71','#27ae60'],
        property: 'background',
        easing: 'linear',
        random: false,
        delay: 5000 // In milliseconds
      });

    window.setTimeout(function() {
        $($$movieCards).children('img').fadeIn(5000);
        }, 2000);

    // $('#placeholder').animate({height: "300px"});

    // User login
    $('.open-it').on('click', function() {
        $('.pop-outer').fadeIn('slow');
    });

    $('.close-it').on('click', function() {
        $('.pop-outer').fadeOut('slow');
    });

    $$inputElement.on('focusout', function(e) {
        userName = e.target.value;
    });

    $('.submit').on('click', function() {
        $('<div class="loader"></div>').appendTo('.pop-inner');
        window.setTimeout(function() {
            // $('#username').animate({right: '300px', opacity: '0.25'});
            // $('#user-password').animate({left: '300px', opacity: '0.25'});
            $('.pop-outer').fadeOut('slow');
            $('.user-login').empty();
            $(`<button type="button" class="btn btn-outline-light">You are logged in as ${userName}</button>`).appendTo('.user-login');
        }, 3000);
    });


    // Load movies db from json
        $.getJSON('db.json', function(data) {
        genreCollection = data.genres;
        moviesCollection = data.movies;
        $.each(genreCollection, function(i, val) {
            var randomColor = btnColor[Math.floor(Math.random()*btnColor.length)];
            $(`<a href="#!${val}"><button type="button" class="btn ${randomColor}" data-genre="${val}">${val}</button></a>`).appendTo('.genres')
        });
    });

    // Render movies by genre
    $$genresContainer.on('click', '.btn', function(e) {
        $($$movieCards).children('img').fadeOut(1000);
        window.setTimeout(function() {
                var activeGenre = e.target.dataset.genre;
                filteredMovies = moviesCollection.reduce(function(acc, movie) {
                    if (movie.genres.includes(activeGenre) === true) {
                        acc.push(movie);
                    }
                    return acc;
                }, []);
        
                $( '.card-columns' ).empty();
                $($$movieCards).children('div').fadeIn(1000);
                $.each(filteredMovies, function(i, val) {
                    $(`<div class="card" data-id="${this.id}"><img src="${this.posterUrl}"></div>`).appendTo('.card-columns');
                });
            }, 500);
    });

    // Show movie details on click
    $$movieCards.on('click', '.card', function(e) {
        var idMovie = e.currentTarget.dataset.id;
        var clickedMovie = filteredMovies.filter(function(item) {
            return item.id == idMovie;
            });

        var left = (screen.width/2)-(650/2);
        var top = (screen.height/2)-(450/2);
        openedWindow = window.open('', 'new_window', 'location=yes, height=450,width=650, top='+top+', left='+left+', scrollbars=yes,status=yes')
        openedWindow.document.title = 'Movie Description';
        openedWindow.document.head.innerHTML = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">';
        openedWindow.document.body.innerHTML =
        Mustache.render(descTemplate, {
            movies: clickedMovie
        });
        openedWindow.focus();

    });

});